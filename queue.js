let collection = [];

// Write the queue functions below.

function print() {
	// output all the elements of the queue

	// this will just print the array named "collection"
	return collection; 

}

function enqueue(element) {
	// add an element to the rear of the queue 
	// push adds "element" into the array "collection"
	collection.push(element);
	return collection;

}

function dequeue() {
	// remove an element at the front of the queue
	// removes first element
	collection.shift();
    return collection;

}

function front() {
	// show the element at the front of the queue
	// return collection.slice(0,1);
	return collection[0];

}

function size() {
	// show the total number of elements 
	return collection.length;
	

}

function isEmpty() {
	// return a Boolean value describing whether the queue is empty or not
	 return collection.length === 0;
}

module.exports = {
	collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};